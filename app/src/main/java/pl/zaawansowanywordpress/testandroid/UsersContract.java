package pl.zaawansowanywordpress.testandroid;

import android.provider.BaseColumns;

/**
 * Created by Q6600 on 07.10.2016.
 */
public final class UsersContract {
    private UsersContract(){}

    public static class UsersEntry implements BaseColumns {
        public static final String TABLE_NAME = "userzy";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_LAST = "last_name";
    }
}
