package pl.zaawansowanywordpress.testandroid;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.lang.String;

public class MainActivity extends AppCompatActivity {

    public String[] data;
    public TextView testText;
    public String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data = new String[]{"tekst 1","tekst 2"};

        Button databaseButton = (Button) findViewById(R.id.databaseButton);
        databaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DbActivity.class);
                startActivity(intent);
            }
        });

        //wczytywanie
        testText = (TextView) findViewById(R.id.testText);
        url = getString(R.string.url);
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()) {
            new DownloadWebpageTask().execute(url);
        } else {
            Toast.makeText(getApplicationContext(), (CharSequence) "Nie ma neta", Toast.LENGTH_LONG).show();
        }
    }

    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                Internet internet = new Internet();
                return internet.downloadUrl(urls[0], data);
            } catch(IOException e) {
                return "Brak strony.";
            } catch(JSONException e) {
                return "Problem z danymi JSON";
            }
        }
        @Override
        protected void onPostExecute(String result) {
            try {

                //JSONObject jsonObject = new JSONObject(result);
                testText.setText(result);

                //Toast.makeText(getApplicationContext(), (CharSequence) "Działa"+posts_array.get(0).toString(), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), (CharSequence) "Nie udało się pobrać: "+e.toString(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
