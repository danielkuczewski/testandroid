package pl.zaawansowanywordpress.testandroid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Internet {


    public String downloadUrl(String myurl, String[] postData) throws IOException,JSONException {
        InputStream is = null;
        int len = 50000;
        try{
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type","application/json");
            conn.setRequestMethod("POST");

            //pakowanie
            JSONArray mJArray = new JSONArray();
            for(String value : postData) {
                mJArray.put(value);
            }
            JSONObject data = new JSONObject();
            data.put("data", mJArray.toString());

            //wysyłanie
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
            writer.write(data.toString());
            writer.close();


            conn.connect();
            int response = conn.getResponseCode();
            is = conn.getInputStream();
            String contentAsString = readIt(is, len);
            return contentAsString;
        } finally {
            if(is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }
}
