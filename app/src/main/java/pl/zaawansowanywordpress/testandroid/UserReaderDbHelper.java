package pl.zaawansowanywordpress.testandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Q6600 on 07.10.2016.
 */
public class UserReaderDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "Testandroid.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + UsersContract.UsersEntry.TABLE_NAME + " (" +
                    UsersContract.UsersEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    UsersContract.UsersEntry.COLUMN_NAME + TEXT_TYPE + ", " +
                    UsersContract.UsersEntry.COLUMN_LAST + TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + UsersContract.UsersEntry.TABLE_NAME;

    public UserReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addUser(String name) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name",name);
        db.insertOrThrow(UsersContract.UsersEntry.TABLE_NAME, null, values);
    }

    public Cursor getAll(){
        String[] columns = {"id","name"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(UsersContract.UsersEntry.TABLE_NAME, columns,
                null, null, null, null, null);
        return cursor;
    }
}
