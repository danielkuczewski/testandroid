package pl.zaawansowanywordpress.testandroid;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class DbActivity extends AppCompatActivity {

    public ListView listView;
    ArrayList<String> tablica1 = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    private UserReaderDbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db);

        listView = (ListView) findViewById(R.id.listView);
        final EditText editText = (EditText) findViewById(R.id.editText);
        db = new UserReaderDbHelper(this);


        //Dodaję elemnty do listy
        adapter = new ArrayAdapter<String>(
                getApplicationContext(), android.R.layout.simple_list_item_1,tablica1
        );
        listView.setAdapter(adapter);

        load_items();

        //event kliknięcia elementu listy
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //usuwanie wybranego elementu

                Toast.makeText(getApplicationContext(), "Kliknięto", Toast.LENGTH_LONG).show();
            }
        });

        Button closeButton = (Button) findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });

        Button dbButton = (Button) findViewById(R.id.addButton);
        dbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String toAdd = editText.getText().toString();
                db.addUser(toAdd);
                tablica1.add(toAdd);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Dodałem", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void load_items(){
        Cursor cursor = db.getAll();
        while (cursor.moveToNext()){
            try {
                try {
                    int number = cursor.getInt(0);
                    tablica1.add(Integer.toString(number));

                    tablica1.add(cursor.getString(1));
                    adapter.notifyDataSetChanged();
                }catch (Exception ee) {
                    //do nothing
                }
            }catch (Exception e){
                TextView errorMessage = (TextView) findViewById(R.id.errorMesage);
                errorMessage.setText(e.toString());
                //Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
